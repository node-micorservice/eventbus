const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");

const app = express();
app.use(bodyParser.json());

app.post("/event", (req, res) => {
  const event = req.body;
  axios.post("https://localhost:4000/events", event);
  axios.post("https://localhost:4001/events", event);
  axios.post("https://localhost:4002/events", event);

  res.send({ status: "OK" });
});

app.listen(4005, () => console.log("listening on 4005"));
